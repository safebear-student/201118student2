package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Created by CCA_Student on 20/11/2018.
 */
public class Utils {
    private static final String URL = System.getProperty("url", "http://localhost:8080");
    private static final String BROWSER = System.getProperty("browser", "chrome");

    public static String getURL() {
        return URL;
    }

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe"); // pointing method to where chromedriver is saved
        ChromeOptions options = new ChromeOptions();  // adding ChromeOptions class
        options.addArguments("window-size=1366,768"); // setting optimal window size for device

        // switch for selecting browsers or using defaults
        switch(BROWSER) {
            case "chrome":
                return new ChromeDriver(options);

                default:
                    return new ChromeDriver(options);
            case "headless":
                options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);
        }
    }
}
