package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by CCA_Student on 21/11/2018.
 */

public class LoginTests extends BaseTests {

    @Test
    public void LoginTest() {
        //Step 1 ACTION: Open our web application in the browser
        driver.get(Utils.getURL());
        Assert.assertEquals(loginPage.getPageTitle(),"Login Page", "The login page didn't open or the title has changed");

        //Step 2 ACTION: Enter Username & Password & click login
        loginPage.login("tester", "letmein");

        //Step 3 EXPECTED RESULT: check that we are now on the tools page
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "The login page didn't open or the title has changed");

    }

}
