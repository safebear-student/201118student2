package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Created by CCA_Student on 21/11/2018.
 */

public abstract class BaseTests { // "abstract" means that this class can be run, only inherited
    WebDriver driver;

    // we are going to create a login/tools page but have not yet created an object. Protected as Login tests will inherit base class
    protected LoginPage loginPage;
    protected ToolsPage toolsPage;

    @BeforeTest // before running tests browser will open
    public void setUp() {
        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
    }


    @AfterTest
    public void tearDown() { // after running test the browser will close
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }
}
