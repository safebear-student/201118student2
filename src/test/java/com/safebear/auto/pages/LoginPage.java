package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

/**
 * Created by CCA_Student on 21/11/2018.
 */

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    public void login(String username, String password) {
        driver.findElement(locators.getUsernameLocator()).sendKeys(username);
        driver.findElement(locators.getPasswordLocator()).sendKeys(password);
        driver.findElement(locators.getLoginButton()).click();
    }

    public String checkUnsuccessfulLoginSuccess() {
        return driver.findElement(locators.getLoginUnsuccessfulMessage()).toString();
    }

}
