package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

/**
 * Created by CCA_Student on 21/11/2018.
 */

@Data
public class ToolsPageLocators {
    private By loginSuccessMessage = By.xpath(".//p[2]/b"); // identifying login message
}
